/*
 * HueColorUtils.cpp
 *
 *  Created on: Nov 25, 2019
 *      Author: mpand
 */

#include "HueColorUtils.h"
#include "math.h"

GamutType HueColorUtils::GamutA(XYPoint(0.703, 0.296), XYPoint(0.214, 0.709), XYPoint(0.139, 0.081));
GamutType HueColorUtils::GamutB(XYPoint(0.674, 0.322), XYPoint(0.408, 0.517), XYPoint(0.168, 0.041));
GamutType HueColorUtils::GamutC(XYPoint(0.692, 0.308), XYPoint(0.17, 0.7), XYPoint(0.153, 0.048));

XYPoint HueColorUtils::RGBToXY(double R, double G, double B, GamutType *pGamut) {
    /* Convert from RGB color to XY color. */
    return RGBToXYBrightness(R, G, B, pGamut);
}

XYBrightness HueColorUtils::RGBToXYBrightness(double R, double G, double B, GamutType *pGamut) {
    /* Convert from RGB color to XY color. */
    if (R + G + B == 0) {
        return XYBrightness(0.0, 0.0, 0);
    }

    // Gamma correction
    R = GAMMA(R);
    G = GAMMA(G);
    B = GAMMA(B);

    // Wide RGB D65 conversion formula
    double X = R * 0.664511 + G * 0.154324 + B * 0.162028;
    double Y = R * 0.283881 + G * 0.668433 + B * 0.047685;
	double Z = R * 0.000088 + G * 0.072310 + B * 0.986039;

    // Convert XYZ to xy
    double x = X / (X + Y + Z);
	double y = Y / (X + Y + Z);

    // Brightness
	if (Y > 1) Y = 1;
    double brightness = Y;

    // Check if the given xy value is within the color-reach of the lamp.
    if (pGamut) {
        bool inReach = CheckPointInLampsReach(XYPoint(x, y), pGamut);
        if (!inReach) {
            XYPoint xyClosest = GetClosestPointToPoint(XYPoint(x, y), pGamut);
            x = xyClosest.x;
            y = xyClosest.y;
        }
    }

    return XYBrightness(x, y, brightness);
}


RGB HueColorUtils::XYToRGB(double x, double y, GamutType *pGamut) {
    /* Convert from XY to a normalized RGB. */
    return XYBrightnessToRGB(x, y, 1.0, pGamut);
}

RGB HueColorUtils::XYBrightnessToRGB(double x, double y, double brightness, GamutType *pGamut) {

    /* Convert from XYZ to RGB. */
    if (pGamut) {
        if (!CheckPointInLampsReach(XYPoint(x, y), pGamut)) {
			XYPoint xyClosest = GetClosestPointToPoint(XYPoint(x, y), pGamut);
			x = xyClosest.x;
			y = xyClosest.y;
        }
    }

    if (brightness == 0.0) {
        return RGB(0, 0, 0);
    }

    double Y = brightness;

    if (y == 0.0) {
        y += 0.00000000001;
    }

    double X = (x * Y) / y;
    double Z = ((1.0 - x - y) * Y) / y;

    // Convert to RGB using Wide RGB D65 conversion.
    double r = X * 1.656492 - Y * 0.354851 - Z * 0.255038;
    double g = -X * 0.707196 + Y * 1.655397 + Z * 0.036152;
	double b = X * 0.051713 - Y * 0.121364 + Z * 1.011530;

    // If one component is greater than 1, weight components by that value.
    double max_component = MAX(MAX(r, g), b);
    if (max_component > 1.0) {
        r /= max_component;
        g /= max_component;
        b /= max_component;
    }

    // Apply reverse gamma correction.
	r = R_GAMMA(r);
	g = R_GAMMA(g);
	b = R_GAMMA(b);

    // Bring all negative components to zero.
    r = MAX(0.0, r);
    g = MAX(0.0, g);
    b = MAX(0.0, b);

    // If one component is greater than 1, weight components by that value.
    max_component = MAX(MAX(r, g), b);
    if (max_component > 1.0) {
        r /= max_component;
        g /= max_component;
        b /= max_component;
    }

    return RGB(r, g, b);
}

RGB HueColorUtils::HSBToRGB(int iH, double dS, double dB) {
    /* Convert a hsb into its rgb representation. */
    if (dS == 0.0) {
        return RGB(dB, dB, dB);
    }

    double r = 0, g = 0, b = 0;

    double h = iH / 60.0;
    double f = h - floor(h);
    double p = dB * (1.0 - dS);
	double q = dB * (1.0 - dS * f);
	double t = dB * (1.0 - (dS * (1.0 - f)));

	switch ((int)h) {
	case 0:
        r = dB;
        g = t;
        b = p;
		break;
	case 1:
        r = q;
        g = dB;
        b = p;
		break;
	case 2:
        r = p;
        g = dB;
        b = t;
		break;
	case 3:
        r = p;
        g = q;
        b = dB;
		break;
	case 4:
        r = t;
        g = p;
        b = dB;
		break;
	case 5:
	default:
        r = dB;
        g = p;
        b = q;
		break;
	}

    return RGB(r, g, b);
}

// S and B in range 0.0 to 1.0. H in range 0 to 360.
HSBrightness HueColorUtils::RGBToHSB(double r, double g, double b) {
	double rgb_min, rgb_max, delta;

	rgb_max = MAX(r, MAX(g, b));
	rgb_min = MIN(r, MIN(g, b));

	HSBrightness h(0, 0.0, rgb_max);

	delta = rgb_max - rgb_min;

	if (rgb_max == 0) {
		h.s = 0;
	} else {
		h.s = delta / rgb_max;
	}

	if (delta == 0) {
		h.h = 0;
		h.s = 0;
	} else if (rgb_max == r) {
		if (g >= b) {
			h.h = round(60.0L * (g - b) / delta);
		} else {
			h.h = round(60.0L*(g-b)/delta + 360);
		}
	} else if (rgb_max == g) {
		h.h = round(60.0L*(b-r)/delta + 120);
	} else {
		h.h = round(60.0L*(r-g)/delta + 240);
	}

	return h;
}

HS HueColorUtils::RGBToHS(double r, double g, double b) {
	return RGBToHSB(r, g, b);
}

RGB HueColorUtils::HSToRGB(int h, double s) {
    /* Convert an hsb color into its rgb representation. */
    return HSBToRGB(h, s, 1.0);
}

HS HueColorUtils::XYToHS(double x, double y, GamutType *pGamut) {
    /* Convert an xy color to its hs representation. */
    RGB rgb = XYToRGB(x, y, pGamut);
    return RGBToHSB(rgb.r, rgb.g, rgb.b);
}

HSBrightness HueColorUtils::XYBrightnessToHSB(double x, double y, double brightness, GamutType *pGamut) {
    /* Convert an xy color to its hs representation. */
    RGB rgb = XYBrightnessToRGB(x, y, brightness, pGamut);
    return RGBToHSB(rgb.r, rgb.g, rgb.b);
}

XYPoint HueColorUtils::HSToXY(int h, double s, GamutType *pGamut) {
    /* Convert an hs color to its xy representation. */
	RGB rgb = HSToRGB(h, s);
    return RGBToXY(rgb.r, rgb.g, rgb.b, pGamut);
}

int HueColorUtils::Bound255(double color_component) {
	return Bound(color_component, 0, 255);
}

int HueColorUtils::Bound(double color_component, double minimum, double maximum) {
    /*
    Bound the given color component value between the given min and max values.

    The minimum and maximum values will be included in the valid output.
    i.e. Given a color_component of 0 and a minimum of 10, the returned value
    will be 10.
    */
    double color_component_out = MAX(color_component, minimum);
    return MIN(color_component_out, maximum);
}

int HueColorUtils::GetRed(double temperature) {
    /* Get the red component of the temperature in RGB space. */
    if (temperature <= 66) {
        return 255;
    }
    double tmp_red = 329.698727446 * pow(temperature - 60, -0.1332047592);
    return Bound255(tmp_red);
}

int HueColorUtils::GetGreen(double temperature) {
    /* Get the green component of the given color temp in RGB space. */
	double green = 0.0;
    if (temperature <= 66) {
        green = 99.4708025861 * log((double)temperature) - 161.1195681661;
    }
    else {
        green = 288.1221695283 * pow(temperature - 60, -0.0755148492);
    }
    return Bound255(green);
}


int HueColorUtils::GetBlue(double temperature) {
    /* Get the blue component of the given color temperature in RGB space. */
    if (temperature >= 66) {
        return 255;
    }

    if (temperature <= 19) {
        return 0;
    }

    double blue = 138.5177312231 * log(temperature - 10) - 305.0447927307;
    return Bound255(blue);
}


double HueColorUtils::TemperatureMiredToKelvin(double mired_temperature) {
    /* Convert absolute mired shift to degrees kelvin. */
    return floor(1000000.0 / mired_temperature);
}

double HueColorUtils::TemperatureKelvinToMired(double kelvin_temperature) {
    /* Convert degrees kelvin to mired shift. */
    return floor(1000000 / kelvin_temperature);
}


// The following 5 functions are adapted from rgbxy provided by Benjamin Knight
// License: The MIT License (MIT), 2014.
// https://github.com/benknight/hue-python-rgb-converter
double HueColorUtils::CrossProduct(XYPoint p1, XYPoint p2) {
    /* Calculate the cross product of two XYPoints. */
    return p1.x * p2.y - p1.y * p2.x;
}

double HueColorUtils::GetDistanceBetweenTwoPoints(XYPoint one, XYPoint two) {
    /* Calculate the distance between two XYPoints. */
    double dx = one.x - two.x;
    double dy = one.y - two.y;
    return (double)sqrt(dx * dx + dy * dy);
}

XYPoint HueColorUtils::GetClosestPointToLine(XYPoint A, XYPoint B, XYPoint P) {
    /*
    Find the closest point from P to a line defined by A and B.

    This point will be reproducible by the lamp
    as it is on the edge of the gamut.
    */

    XYPoint AP = XYPoint(P.x - A.x, P.y - A.y);
	XYPoint AB = XYPoint(B.x - A.x, B.y - A.y);
    double ab2 = AB.x * AB.x + AB.y * AB.y;
    double ap_ab = AP.x * AB.x + AP.y * AB.y;
    double t = ap_ab / ab2;

    if (t < 0.0)
        t = 0.0;
    else if (t > 1.0)
        t = 1.0;

    return XYPoint(A.x + AB.x * t, A.y + AB.y * t);
}


XYPoint HueColorUtils::GetClosestPointToPoint(XYPoint p, GamutType *pGamut) {
    /*
    Get the closest matching color within the gamut of the light.

    Should only be used if the supplied color is outside of the color gamut.
    */
    // find the closest point on each line in the CIE 1931 'triangle'.
    XYPoint pAB = GetClosestPointToLine(pGamut->red, pGamut->green, p);
    XYPoint pAC = GetClosestPointToLine(pGamut->blue, pGamut->red, p);
    XYPoint pBC = GetClosestPointToLine(pGamut->green, pGamut->blue, p);

    // Get the distances per point and see which point is closer to our Point.
	double dAB = GetDistanceBetweenTwoPoints(p, pAB);
	double dAC = GetDistanceBetweenTwoPoints(p, pAC);
	double dBC = GetDistanceBetweenTwoPoints(p, pBC);

    double lowest = dAB;
    XYPoint closest_point = pAB;

    if (dAC < lowest) {
        lowest = dAC;
        closest_point = pAC;
    }

    if (dBC < lowest) {
        lowest = dBC;
        closest_point = pBC;
    }

    // Change the xy value to a value which is within the reach of the lamp.
    double cx = closest_point.x;
    double cy = closest_point.y;

    return XYPoint(cx, cy);
}


bool HueColorUtils::CheckPointInLampsReach(XYPoint p, GamutType *pGamut) {
    /* Check if the provided XYPoint can be recreated by a Hue lamp. */
	XYPoint v1 = XYPoint(pGamut->green.x - pGamut->red.x, pGamut->green.y - pGamut->red.y);
	XYPoint v2 = XYPoint(pGamut->blue.x - pGamut->red.x, pGamut->blue.y - pGamut->red.y);

	XYPoint q = XYPoint(p.x - pGamut->red.x, p.y - pGamut->red.y);
    double s = CrossProduct(q, v2) / CrossProduct(v1, v2);
    double t = CrossProduct(v1, q) / CrossProduct(v1, v2);

    return (s >= 0.0) and (t >= 0.0) and (s + t <= 1.0);
}


bool HueColorUtils::CheckValidGamut(GamutType &gamut) {
    /* Check if the supplied gamut is valid. */
    // Check if the three points of the supplied gamut are not on the same line.
    XYPoint v1 = XYPoint(gamut.green.x - gamut.red.x, gamut.green.y - gamut.red.y);
	XYPoint v2 = XYPoint(gamut.blue.x - gamut.red.x, gamut.blue.y - gamut.red.y);
    bool not_on_line = CrossProduct(v1, v2) > 0.0001;

    // Check if all six coordinates of the gamut lie between 0 and 1.
    bool red_valid = (
        gamut.red.x >= 0 and gamut.red.x <= 1 and gamut.red.y >= 0 and gamut.red.y <= 1
    );
    bool green_valid = (
        gamut.green.x >= 0
        and gamut.green.x <= 1
        and gamut.green.y >= 0
        and gamut.green.y <= 1
    );
    bool blue_valid = (
        gamut.blue.x >= 0
        and gamut.blue.x <= 1
        and gamut.blue.y >= 0
        and gamut.blue.y <= 1
    );

    return not_on_line and red_valid and green_valid and blue_valid;
}
