/*
 * HueColorUtils.h
 *
 *  Created on: Nov 25, 2019
 *      Author: mpand
 */

#ifndef LIBRARIES_FAUXMOESP_HUECOLORUTILS_H_
#define LIBRARIES_FAUXMOESP_HUECOLORUTILS_H_

#define MAX(a,b) ((a) > (b) ? (a) : (b))
#define MIN(a,b) ((a) > (b) ? (b) : (a))

#define GAMMA(a) (a > 0.04045 ? pow((a + 0.055) / (1.0 + 0.055), 2.4) : (a / 12.92))
#define R_GAMMA(a) (a > 0.0031308 ? ((1.0 + 0.055) * pow(a, (1.0 / 2.4)) - 0.055) : (a * 12.92))

struct XYPoint {
    /* Represents a CIE 1931 XY coordinate pair. */
	XYPoint(double x, double y) : x(x), y(y) {}

    double x;	// 0.0..1.0
    double y;	// 0.0..1.0
};

struct XYBrightness : public XYPoint {
	XYBrightness(double x, double y, double bri) : XYPoint(x, y), bri(bri) {}

	double bri;	// 0.0..1.0
};

struct HS {
	HS(int h, double s) : h(h), s(s) {}

    int h;		// 0..360
    double s;	// 0.0..1.0
};

struct HSBrightness : public HS {
	HSBrightness(int h, double s, double bri) : HS(h, s), bri(bri) {}

	double bri;	// 0.0..1.0
};

struct RGB {
	RGB(double r, double g, double b) : r(r), g(g), b(b) {}

	double r;	// 0..1
	double g;	// 0..1
	double b;	// 0..1
};

struct GamutType {
    /* Represents the Gamut of a light. */
	GamutType(XYPoint red, XYPoint green, XYPoint blue) : red(red), green(green), blue(blue) {}

    // ColorGamut = gamut(xypoint(xR,yR),xypoint(xG,yG),xypoint(xB,yB))
    XYPoint red;
	XYPoint green;
	XYPoint blue;
};


class HueColorUtils {
public:
	static GamutType GamutA;
	static GamutType GamutB;
	static GamutType GamutC;

	static XYPoint RGBToXY(double R, double G, double B, GamutType *pGamut);

	// Taken from:
	// http://www.developers.meethue.com/documentation/color-conversions-rgb-xy
	// License: Code is given as is. Use at your own risk and discretion.
	static XYBrightness RGBToXYBrightness(double R, double G, double B, GamutType *pGamut);

	static RGB XYToRGB(double x, double y, GamutType *pGamut);

	// Converted to Python from Obj-C, original source from:
	// http://www.developers.meethue.com/documentation/color-conversions-rgb-xy
	static RGB XYBrightnessToRGB (double x, double y, double brightness, GamutType *pGamut);

	// fH in range 0 to 360, s and b in range 0.0 to 1.0.
	static RGB HSBToRGB(int fH, double fS, double fB);

	// S and B in range 0.0 to 1.0. H in range 0 to 360.
	static HSBrightness RGBToHSB(double R, double G, double B);
	static HS RGBToHS(double R, double G, double B);
	static RGB HSToRGB(int h, double s);
	static HS XYToHS(double x, double y, GamutType *pGamut);
	static HSBrightness XYBrightnessToHSB(double x, double y, double brightness, GamutType *pGamut);
	static XYPoint HSToXY(int h, double s, GamutType *pGamut);
	static HS TemperatureToHS(double kelvin);

private:
	static int Bound255(double color_component);
	static int Bound(double color_component, double minimum, double maximum);
	static int GetRed(double temperature);
	static int GetGreen(double temperature);
	static int GetBlue(double temperature);
	static double TemperatureMiredToKelvin(double mired_temperature);
	static double TemperatureKelvinToMired(double kelvin_temperature);
	// The following 5 functions are adapted from rgbxy provided by Benjamin Knight
	// License: The MIT License (MIT), 2014.
	// https://github.com/benknight/hue-python-rgb-converter
	static double CrossProduct(XYPoint p1, XYPoint p2);
	static double GetDistanceBetweenTwoPoints(XYPoint one, XYPoint two);
	static XYPoint GetClosestPointToLine(XYPoint A, XYPoint B, XYPoint P);
	static XYPoint GetClosestPointToPoint(XYPoint p, GamutType *pGamut);
	static bool CheckPointInLampsReach(XYPoint p, GamutType *pGamut);
	static bool CheckValidGamut(GamutType &gamut);
};

#endif /* LIBRARIES_FAUXMOESP_HUECOLORUTILS_H_ */
